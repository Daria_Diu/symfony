<?php

use \Codeception\Util\HttpCode;

class CustomerCest
{
    // tests
    public function createCustomer(ApiTester $I)
    {
        $data = [
            'name' => 'name',
            'firstname'=> 'firstname',
            'lastname' => 'lastname',
            'phone'=> '12345',
            'email' => 'davert@codeception.com'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', $data);

        $I->seeResponseCodeIs(HttpCode::CREATED); //201 какой код ответа endpoint на который мы обратились (CREATED - константа)
        $I->seeResponseIsJson(); //проверяет json это или нет
        $I->seeResponseContains(json_encode($data)); //сравнивает данные json_encode($data) с данными пришедшими из '/api/customer'
    }

    public function getCustomer(\ApiTester $I){
        $data = [
            'name' => 'name',
            'firstname'=> 'firstname',
            'lastname' => 'lastname',
            'phone'=> '12345',
            'email' => 'davert@codeception.com'
        ];

        $I->sendGet('/api/customer/21da53ef-209a-11ec-b719-2c600c85b4f4');

        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode($data));

    }

    public function deleteCustomer(\ApiTester $I){
        $I->sendDelete('/api/customer/afa7e486-2098-11ec-b719-2c600c85b4f4');
        $I->seeResponseCodeIs(HttpCode::OK); // 200

        $I->sendGet('/api/customer/afa7e486-2098-11ec-b719-2c600c85b4f4');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404
    }

    public function updateCustomer(\ApiTester $I){
        $data = [
            'name' =>' Anna',
            'firstname' => 'Semenovich',
            'lastname' => 'Ivanovna',
            'phone' => '123456789',
            'email' => 'anna@codeception.com'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/api/customer/d09ea7c7-2085-11ec-b719-2c600c85b4f4', $data);

        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseContains(json_encode($data));
    }

    public function getCustomers(\ApiTester $I){
        $I->sendGet('/api/customers');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
    }

    //negative tests methods
    public function negativeCreateCustomer(\ApiTester $I){
        $data1 = [
            'name' => 'Vasia',
            'firstname' => 'Pupkin',
        ];

        $data2 = [
            'name' => 'Vasia',
            'firstname' => 'Pupkin',
            'lastname' => '',
            'phone' => '123',
            'email' => 'pupkin@mail.com'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', $data1);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST); // 400

        $I->sendPost('/api/customer', $data2);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST); // 400
    }

    public function negativeGetCustomer(\ApiTester $I){
        $I->sendGet('/api/customer/11111111111111111111111');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404

        $I->sendGet('/api/customer/ ');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404

        $I->sendGet('/api/customer/qwerty');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404

        $I->sendGet('/api/customer/');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404
    }

    public function negativeUpdateCustomer(\ApiTester $I){
        $data1 = [
            'phone' => '123',
            'email' => 'jjjj@codeception.com'
        ];

        $data2 = [
            'name' =>' Anna',
            'firstname' => 'Semenovich',
            'lastname' => 'Ivanovna',
            'phone' => '123456789',
            'email' => ''
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/api/customer/d09ea7c7-2085-11ec-b719-2c600c85b4f4', $data1);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY); // 422

        $I->sendPut('/api/customer/d09ea7c7-2085-11ec-b719-2c600c85b4f4', $data2);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY); // 422
    }

    public function negativeDeleteCustomer(\ApiTester $I){
        $I->sendDelete('/api/customer/1111111111111');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404

        $I->sendDelete('/api/customer/');
        $I->seeResponseCodeIs(HttpCode:: NOT_FOUND); // 404

        $I->sendDelete('/api/customer');
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED); // 405
    }

    public function negativeDeleteCustomers(\ApiTester $I){
        $I->sendDelete('/api/customers');
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED); // 405
    }

}
